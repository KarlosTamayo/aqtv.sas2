﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="aqtv.sas2.web.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>





  
    <div class="row">
        <div class="col-md-5"></div>
        <div class="col-md-4" > 
            <asp:Image ID="Logo" runat="server" Height="124px" ImageUrl="~/Image/LogoAQTVSAS.png" Width="140px" />
        </div>
        <div class="col-md-3"></div>
    </div>




     <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
          <h3 >Visión</h3>
        </div>
        <div class="col-md-1"></div>
    </div>
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            
            <h4>  <a>&raquo;</a>
                La visión de la empresa como tal es a 10 años tener como mínimo 3 sucursales con su infraestructura propia y que estas cuenten con totas las herramientas tecnológicas que les permita proyectarse regional, nacional e internacionalmente.
                     De igual forma contar por lo menos con un número suficiente de vehículos que nos permita como empresa responder oportunamente al momento de satisfacer las necesidades y requerimientos de los usuarios o clientes.
                          Contar con personal debidamente capacitado en Análisis y Sistemas de Información, que nos brinden como empresa las garantías y la mejor imagen en el campo empresarial.
                   Contar con un buen sistema contable que que nos permita agilizar y optimizar todas las actividades financieras de la empresa.
             
                </h4>
              
        </div>
        <div class="col-md-1"></div>
    </div>

    
     <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
          <h3 >Misión</h3>
        </div>
        <div class="col-md-1"></div>
    </div>
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <h4>  <a>&raquo;</a>
                Como misión la empresa AQTVSAS tiene, solucionar las necesidades de la gran mayoría de usuarios tanto del sector público, como del sector privado a través de los productos y servicios ofrecidos como lo son.<br />
                                •	Internet <br />
                                •	Televisión satelital <br />
                                •	Dotación e instalación de cámaras de seguridad <br />
                                 •	Energía solar y eólica <br />
                                •	Todo lo concerniente a servicios de electricidad.   <br />

            </h4>
        </div>
        <div class="col-md-1"></div>
    </div>


  
</asp:Content>
