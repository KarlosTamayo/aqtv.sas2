﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace aqtv.sas2.web
{
    public partial class Contact : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ArrayList lista = new ArrayList();
            string[] Achivos = System.IO.Directory.GetFiles(Server.MapPath("~/Imagenes"), "*.*");

            foreach (string archivos in Achivos)
            {
                lista.Add("/Imagenes/" + System.IO.Path.GetFileName(archivos));
            }
            Repeater1.DataSource = lista;
            Repeater1.DataBind();

        }
    }
}