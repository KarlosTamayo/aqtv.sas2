﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TipoDeServicio.aspx.cs" Inherits="aqtv.sas2.web.WebForm5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#tabs" ).tabs();
  } );
  </script>


   
      



    <div id="tabs">
  <ul>
    <li><a href="#tabs-1">Ingresar tipo Servicio</a></li>
    <li><a href="#tabs-2">lista servicios</a></li>
    
  </ul>
  <div id="tabs-1">
 <h1>Tipo de serviso</h1>
    <table style="width: 44%">
        <tr>
            <td style="width: 128px">Descripcion:</td>
            <td>
                <asp:TextBox ID="DescripcionTextBox" runat="server" Width="203px"></asp:TextBox>
            </td>
        </tr>
    </table>
    <p>&nbsp;</p>
        <asp:Button ID="AgregarButton" runat="server" OnClick="AgregarButton_Click" Text="Agregar" />
    <p>&nbsp;</p>
    <p><strong>
        <asp:Label ID="MensajeLabel" runat="server" style="color: #0000CC; font-size: x-large"></asp:Label>
        </strong></p>
    <p>&nbsp;</p>
    <p>  </div>
  <div id="tabs-2">
  <asp:GridView ID="TipoDeServicioGridView" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="TipodeServicioID" DataSourceID="TipoDeServicioSqlDataSource" ForeColor="#333333" GridLines="None" AllowPaging="True" AllowSorting="True" Width="469px">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowSelectButton="True" />
                <asp:BoundField DataField="TipodeServicioID" HeaderText="Tipo de Servicio" InsertVisible="False" ReadOnly="True" SortExpression="TipodeServicioID" />
                <asp:BoundField DataField="Descripcion" HeaderText="Descripción" SortExpression="Descripcion" />
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>
        <asp:SqlDataSource ID="TipoDeServicioSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT [TipodeServicioID], [Descripcion] FROM [TipodeServicio]" DeleteCommand="delete from TipodeServicio where TipodeServicioID =@TipodeServicioID" InsertCommand="INSERT INTO TipodeServicio(TipodeServicioID, Descripcion) VALUES (@TipodeServicioID,@Descripcion)" UpdateCommand="UPDATE TipodeServicio SET Descripcion =@Descripcion where TipodeServicioID =@TipodeServicioID">
            <DeleteParameters>
                <asp:Parameter Name="TipodeServicioID" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="TipodeServicioID" />
                <asp:Parameter Name="Descripcion" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="Descripcion" />
                <asp:Parameter Name="TipodeServicioID" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>  </div>

</div>



    

</asp:Content>
