﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Clientes.aspx.cs" Inherits="aqtv.sas2.web.WebForm3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


  
    
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">       
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="/resources/demos/style.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>
            $(function () {
                $(".FechaTextBox").datepicker({
                    dateFormat: "yy/mm/dd",
                    changeYear: true
                });
                 $( "#tabs" ).tabs();
            });
        </script>
    
   



   



    <div id="tabs">
  <ul>
    <li><a href="#tabs-1">Nuevo cliente</a></li>
    <li><a href="#tabs-2">Lista de clientes</a></li>
    
  </ul>
  <div id="tabs-1">

    <h1>Clientes</h1>
    <table style="width: 100%">
        <tr>
            <td class="modal-sm" style="width: 161px; height: 32px">TipoDocumentoID:</td>
            <td class="modal-sm" style="width: 262px; height: 32px">
                <asp:DropDownList ID="TipoDocumentoDropDownList" runat="server" DataSourceID="TipoDocumentoSqlDataSource" DataTextField="Descripcion" DataValueField="TipoDocumentoID" Height="27px" Width="229px">
                </asp:DropDownList>
            </td>
            <td style="width: 147px; height: 32px">Identificacion</td>
            <td style="height: 32px">
                <asp:TextBox ID="IdentificacionTextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="modal-sm" style="width: 161px; height: 32px">Nombre:</td>
            <td class="modal-sm" style="width: 262px; height: 32px">
                <asp:TextBox ID="NombreTextBox" runat="server"></asp:TextBox>
            </td>
            <td style="width: 147px; height: 32px">Apellido</td>
            <td style="height: 32px">
                <asp:TextBox ID="ApellidoTextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="modal-sm" style="width: 161px; height: 33px">Dirección</td>
            <td class="modal-sm" style="width: 262px; height: 33px">
                <asp:TextBox ID="DireccionTextBox" runat="server"></asp:TextBox>
            </td>
            <td style="width: 147px; height: 33px">Celular</td>
            <td style="height: 33px">
                <asp:TextBox ID="CelularTextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="modal-sm" style="width: 161px">Correo</td>
            <td class="modal-sm" style="width: 262px">
                <asp:TextBox ID="CorreoTextBox" runat="server"></asp:TextBox>
            </td>

            <td style="width: 147px">Fecha</td>
            <td>
                <asp:TextBox ID="FechaTextBox" runat="server" CssClass="FechaTextBox"></asp:TextBox>
            </td>
        </tr>
    </table>


    <p>&nbsp;</p>
    <p>
        <asp:Button ID="AgregarButton" runat="server" Text="Agregar cliente" OnClick="AgregarButton_Click" />
    </p>
    <p>
        <strong>
            <asp:Label ID="MensajeLabel" runat="server" Style="font-size: x-large; color: #0000CC"></asp:Label>
        </strong>
    </p>  </div>

  <div id="tabs-2">
 <p>
        <asp:GridView ID="ClientesGridView" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ClientesID" DataSourceID="ClienteSqlDataSource" ForeColor="#333333" GridLines="None" Width="507px">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowSelectButton="True" ButtonType="Image" DeleteImageUrl="~/Image/icon/delete.png" EditImageUrl="~/Image/icon/edit.png" SelectImageUrl="~/Image/icon/select.png" UpdateImageUrl="~/Image/icon/save.png" />
                <asp:BoundField DataField="ClientesID" HeaderText="Clientes" InsertVisible="False" ReadOnly="True" SortExpression="ClientesID" />
                <asp:BoundField DataField="TipoDocumentoID" HeaderText="Documento" SortExpression="TipoDocumentoID" />
                <asp:BoundField DataField="Identificacion" HeaderText="Identificación" SortExpression="Identificacion" />
                <asp:BoundField DataField="Nombre" HeaderText="Nombre" SortExpression="Nombre" />
                <asp:BoundField DataField="Apellido" HeaderText="Apellido" SortExpression="Apellido" />
                <asp:BoundField DataField="Direccion" HeaderText="Dirección" SortExpression="Direccion" />
                <asp:BoundField DataField="Celular" HeaderText="Celular" SortExpression="Celular" />
                <asp:BoundField DataField="Correo" HeaderText="Correo" SortExpression="Correo" />
                <asp:BoundField DataField="Fecha" HeaderText="Fecha" SortExpression="Fecha" />
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>
        <asp:SqlDataSource ID="ClienteSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT [ClientesID], [TipoDocumentoID], [Identificacion], [Nombre], [Apellido], [Direccion], [Celular], [Correo], [Fecha] FROM [Clientes]" DeleteCommand="DELETE FROM [Clientes] WHERE [ClientesID] = @ClientesID" InsertCommand="
INSERT INTO [Clientes] ([TipoDocumentoID], [Documento], [Nombre], [Apellidos], [Direccion], [Celular], [Correo], [Fecha]) VALUES (@TipoDocumentoID, @Documento, @Nombre, @Apellidos, @Direccion, @Celular, @Correo, @Fecha)
"
            UpdateCommand="UPDATE [Clientes] SET [TipoDocumentoID] = @TipoDocumentoID,[Identificacion] = @Identificacion, [Nombre] = @Nombre, [Apellido] = @Apellido, [Direccion] = @Direccion, [Celular] = @Celular, [Correo] = @Correo, [Fecha] = @Fecha
 WHERE [ClientesID] = @ClientesID">
            <DeleteParameters>
                <asp:Parameter Name="ClientesID" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="TipoDocumentoID" />
                <asp:Parameter Name="Documento" />
                <asp:Parameter Name="Nombre" />
                <asp:Parameter Name="Apellidos" />
                <asp:Parameter Name="Direccion" />
                <asp:Parameter Name="Celular" />
                <asp:Parameter Name="Correo" />
                <asp:Parameter Name="Fecha" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="TipoDocumentoID" />
                <asp:Parameter Name="Identificacion" />
                <asp:Parameter Name="Nombre" />
                <asp:Parameter Name="Apellido" />
                <asp:Parameter Name="Direccion" />
                <asp:Parameter Name="Celular" />
                <asp:Parameter Name="Correo" />
                <asp:Parameter Name="Fecha" />
                <asp:Parameter Name="ClientesID" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </p>
    <p>&nbsp;</p>
    <p>
        <asp:SqlDataSource ID="TipoDocumentoSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
            SelectCommand="select TipoDocumentoID,Descripcion from TipoDocumento union
select 0,'[Escoge tipo de documento]'
order by 2"></asp:SqlDataSource>
    </p>
    <p>
        &nbsp;
    </p>
  </div>

</div>

</asp:Content>
