﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Proveedor.aspx.cs" Inherits="aqtv.sas2.web.WebForm7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">  
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#tabs" ).tabs();
  } );
  </script>


   




    <div id="tabs">
  <ul>
    <li><a href="#tabs-1">Proveedor</a></li>
    <li><a href="#tabs-2">Lista de pr</a></li>
    
  </ul>
  <div id="tabs-1">
 <h1>Proveedor</h1>
    <p>&nbsp;</p>
    <table style="width: 100%">
        <tr>
            <td style="width: 161px; height: 22px">TipoDocumentoID</td>
            <td class="modal-sm" style="width: 235px; height: 22px">
                <asp:DropDownList ID="TipoDocumentoDropDownList" runat="server" DataSourceID="TipoDocumentoSqlDataSource" DataTextField="Descripcion" DataValueField="TipoDocumentoID" Height="27px" Width="200px">
                </asp:DropDownList>
            </td>
            <td style="width: 149px; height: 22px">Documento</td>
            <td style="height: 22px">
                <asp:TextBox ID="DocumentoTextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 161px">Nombre Comercial</td>
            <td class="modal-sm" style="width: 235px">
                <asp:TextBox ID="NombreComercialTextBox" runat="server"></asp:TextBox>
            </td>
            <td style="width: 149px">Nombre Contacto</td>
            <td>
                <asp:TextBox ID="NombreContactoTextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 161px">ApellidoContacto</td>
            <td class="modal-sm" style="width: 235px">
                <asp:TextBox ID="ApellidoContactoTextBox" runat="server"></asp:TextBox>
            </td>
            <td style="width: 149px">Direccion</td>
            <td>
                <asp:TextBox ID="DireccionTextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 161px; height: 22px">Celular</td>
            <td class="modal-sm" style="width: 235px; height: 22px">
                <asp:TextBox ID="CelularTextBox" runat="server"></asp:TextBox>
            </td>
            <td style="width: 149px; height: 22px">Correo</td>
            <td style="height: 22px">
                <asp:TextBox ID="CorreoTextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 161px">&nbsp;</td>
            <td class="modal-sm" style="width: 235px">&nbsp;</td>
            <td style="width: 149px">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
    <p>&nbsp;</p>
    <p>
        <asp:Button ID="ProveedorButton" runat="server" OnClick="ProveedorButton_Click" Text="Agregar proveedor" />
    </p>
    <p><strong>
        <asp:Label ID="MensajeLabel" runat="server" style="font-size: x-large; color: #0000CC"></asp:Label>
        </strong>

    </p>  </div>
  <div id="tabs-2">
    <p>
        <asp:GridView ID="proveedorGridView" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ProveedorID" DataSourceID="ProveedorSqlDataSource" ForeColor="#333333" GridLines="None" Width="16px" Height="194px">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowSelectButton="True" />
                <asp:BoundField DataField="ProveedorID" HeaderText="Proveedor" InsertVisible="False" ReadOnly="True" SortExpression="ProveedorID" />
                <asp:BoundField DataField="TipoDocumentoID" HeaderText="Documento" SortExpression="TipoDocumentoID" />
                <asp:BoundField DataField="Documento" HeaderText="Documento" SortExpression="Documento" />
                <asp:BoundField DataField="NombreComercial" HeaderText="N_Comercial" SortExpression="NombreComercial" />
                <asp:BoundField DataField="NombreContacto" HeaderText="N_Contacto" SortExpression="NombreContacto" />
                <asp:BoundField DataField="ApellidoContacto" HeaderText="A_Contacto" SortExpression="ApellidoContacto" />
                <asp:BoundField DataField="Direccion" HeaderText="Direccion" SortExpression="Direccion" />
                <asp:BoundField DataField="Celular" HeaderText="Celular" SortExpression="Celular" />
                <asp:BoundField DataField="Correo" HeaderText="Correo" SortExpression="Correo" />
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>
        <asp:SqlDataSource ID="ProveedorSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" DeleteCommand="delete from Proveedor where  ProveedorID =@ProveedorID" InsertCommand="INSERT INTO Proveedor   (ProveedorID, TipoDocumentoID, Documento, NombreComercial, NombreContacto, ApellidoContacto, Direccion, Celular, Correo)
VALUES  (@TipoDocumentoID,@Documento,  @NombreComercial,  @NombreContacto, @ApellidoContacto, @Direccion, @Celular, @Correo)" SelectCommand="SELECT [ProveedorID], [TipoDocumentoID], [Documento], [NombreComercial], [NombreContacto], [ApellidoContacto], [Direccion], [Celular], [Correo] FROM [Proveedor]" UpdateCommand="UPDATE Proveedor SET TipoDocumentoID =@TipoDocumentoID , Documento =@Documento , NombreComercial =@NombreComercial, NombreContacto =@NombreContacto , ApellidoContacto =@ApellidoContacto, Direccion =@Direccion, Celular =@Celular, Correo =@Correo where ProveedorID =@ProveedorID ">
            <DeleteParameters>
                <asp:Parameter Name="ProveedorID" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="TipoDocumentoID" />
                <asp:Parameter Name="Documento" />
                <asp:Parameter Name="NombreComercial" />
                <asp:Parameter Name="NombreContacto" />
                <asp:Parameter Name="ApellidoContacto" />
                <asp:Parameter Name="Direccion" />
                <asp:Parameter Name="Celular" />
                <asp:Parameter Name="Correo" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="TipoDocumentoID" />
                <asp:Parameter Name="Documento" />
                <asp:Parameter Name="NombreComercial" />
                <asp:Parameter Name="NombreContacto" />
                <asp:Parameter Name="ApellidoContacto" />
                <asp:Parameter Name="Direccion" />
                <asp:Parameter Name="Celular" />
                <asp:Parameter Name="Correo" />
                <asp:Parameter Name="ProveedorID" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </p>
    <p>&nbsp;</p>
    <p>
        <asp:SqlDataSource ID="TipoDocumentoSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select TipoDocumentoID,Descripcion from TipoDocumento
     union
     select 0, '[Seleccina un tipo documento ]'
     order by 2"></asp:SqlDataSource>
    </p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>  </div>

</div>

</asp:Content>
