﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TipoDocumento.aspx.cs" Inherits="aqtv.sas2.web.WebForm2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
 
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#tabs" ).tabs();
  } );
  </script>




 



    <div id="tabs">
  <ul>
    <li><a href="#tabs-1">Ingresar tipo Documento</a></li>
    <li><a href="#tabs-2">Lista tipo Documento</a></li>
   
  </ul>
  <div id="tabs-1">
    <h1>Tipo De Documento</h1>
    <table style="width: 44%">
        <tr>
            <td style="width: 128px">Descripcion:</td>
            <td>
                <asp:TextBox ID="DescripcionTextBox" runat="server" Width="203px"></asp:TextBox>
            </td>
        </tr>
    </table>
<p>&nbsp;</p>
    <p>
        <asp:Button ID="AgregarButton" runat="server" OnClick="AgregarButton_Click" Text="Agregar" />
    </p>
    <p><strong>
        <asp:Label ID="MensajeLabel" runat="server" style="color: #0000CC; font-size: x-large"></asp:Label>
        </strong></p>
<p>  </div>
  <div id="tabs-2">
   <asp:GridView ID="TipoDocumentoGridView" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="TipoDocumentoID" DataSourceID="TipoDocumetnoSqlDataSource" ForeColor="#333333" GridLines="None" Width="446px">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowSelectButton="True" />
            <asp:BoundField DataField="TipoDocumentoID" HeaderText="TipoDocumentoID" InsertVisible="False" ReadOnly="True" SortExpression="TipoDocumentoID" />
            <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" SortExpression="Descripcion" />
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
    <asp:SqlDataSource ID="TipoDocumetnoSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" DeleteCommand="
DELETE FROM [TipoDocumento]  WHERE [TipoDocumentoID] = @TipoDocumentoID" InsertCommand="INSERT INTO [TipoDocumento]  ([Descripcion]) VALUES (@Descripcion)" SelectCommand="SELECT [TipoDocumentoID], [Descripcion] FROM [TipoDocumento]" UpdateCommand="UPDATE [TipoDocumento] SET [Descripcion] =@Descripcion where [TipoDocumentoID] =@TipoDocumentoID">
        <DeleteParameters>
            <asp:Parameter Name="TipoDocumentoID" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Descripcion" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Descripcion" />
            <asp:Parameter Name="TipoDocumentoID" />
        </UpdateParameters>
    </asp:SqlDataSource>
    </p>
    <p>
        &nbsp;</p>  </div>

</div>




</asp:Content>
