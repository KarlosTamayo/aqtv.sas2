﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="productos.aspx.cs" Inherits="aqtv.sas2.web.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#tabs" ).tabs();
  } );
  </script>
    
    
    
   

    <div id="tabs">
  <ul>
    <li><a href="#tabs-1">Agregar producto</a></li>
    <li><a href="#tabs-2">lista productos</a></li>
   
  </ul>
  <div id="tabs-1">
<h1>
        Productos
    </h1>
    <table class="nav-justified">
        <tr>
            <td style="width: 135px">Descripción:</td>
            <td>
                <asp:TextBox ID="DescripcionTextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 135px">Precio</td>
            <td>
                <asp:TextBox ID="PrecioTextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 135px">Stok</td>
            <td>
                <asp:TextBox ID="StokTextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
    </table>
    <p>
        &nbsp;</p>
    <asp:Button ID="AgregarButton" runat="server" OnClick="AgregarButton_Click" Text="Agregar producto" />
    <br />
    <br />
    <asp:Label ID="mensajeLabel" runat="server" style="font-size: x-large; color: #000099; font-weight: 700"></asp:Label>
    <br />
<br />  </div>
  <div id="tabs-2">
 <br />
    <asp:GridView ID="ProductoGridView" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ProductoID" DataSourceID="ProductoSqlDataSource" EmptyDataText="There are no data records to display." ForeColor="#333333" GridLines="None" Width="576px">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowSelectButton="True" ButtonType="Image" DeleteImageUrl="~/Image/icon/delete.png" EditImageUrl="~/Image/icon/edit.png" SelectImageUrl="~/Image/icon/select.png" UpdateImageUrl="~/Image/icon/save.png" />
            <asp:BoundField DataField="ProductoID" HeaderText="Producto ID" ReadOnly="True" SortExpression="ProductoID" />
            <asp:BoundField DataField="Descripcion" HeaderText="Descripción" SortExpression="Descripcion" />
            <asp:BoundField DataField="Precio" DataFormatString="{0:N1}" HeaderText="Precio" SortExpression="Precio">
            <ItemStyle HorizontalAlign="Left" />
            </asp:BoundField>
            <asp:BoundField DataField="Stok" DataFormatString="{0:N1}" HeaderText="Stok" SortExpression="Stok">
            <ItemStyle HorizontalAlign="Left" />
            </asp:BoundField>
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
</asp:GridView>
<asp:SqlDataSource ID="ProductoSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" DeleteCommand="DELETE FROM [Producto] WHERE [ProductoID] = @ProductoID" InsertCommand="INSERT INTO [Producto] ([Descripcion], [Precio], [Stok]) VALUES (@Descripcion, @Precio, @Stok)" SelectCommand="SELECT [ProductoID], [Descripcion], [Precio], [Stok] FROM [Producto]" UpdateCommand="UPDATE [Producto] SET [Descripcion] = @Descripcion, [Precio] = @Precio, [Stok] = @Stok WHERE [ProductoID] = @ProductoID">
    <DeleteParameters>
        <asp:Parameter Name="ProductoID" Type="Int32" />
    </DeleteParameters>
    <InsertParameters>
        <asp:Parameter Name="Descripcion" Type="String" />
        <asp:Parameter Name="Precio" Type="Decimal" />
        <asp:Parameter Name="Stok" Type="Double" />
    </InsertParameters>
    <UpdateParameters>
        <asp:Parameter Name="Descripcion" Type="String" />
        <asp:Parameter Name="Precio" Type="Decimal" />
        <asp:Parameter Name="Stok" Type="Double" />
        <asp:Parameter Name="ProductoID" Type="Int32" />
    </UpdateParameters>
</asp:SqlDataSource>
    <br />
    <br />
    <br />
    <br />  </div>
 
</div>


</asp:Content>
