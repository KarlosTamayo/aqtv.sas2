﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="aqtv.sas2.web._Default" %>







<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server" OnClientClick="document.body.style.backgroundColor='#ff0000'; return false;">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $("#accordion").accordion();
        });
    </script>



    <div class="jumbotron">
        <div class="col-md-4">
            <h1>AQTV.SAS</h1>
        </div>
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
            <asp:Image ID="Logo" runat="server" Height="124px" ImageUrl="~/Image/LogoAQTVSAS.png" Width="140px" />

        </div>
        <p><a>&raquo;</a> objetivo mostrar al usuario cómo entrar en el sistema de información de la empresa AQTVSAS, lo que permite conocer,
            de forma eficaz y sencilla todas las operaciones básicas que este ofrece y cuál es el contenido y la funcionalidad de cada uno de 
            los módulos que componen su forma estructural.

        </p>
    </div>


                
  
  

    <div id="accordion" >
        <h3>Inicio</h3>
        <div>
            <p>

                <p>
                    <asp:LinkButton ID="ClientesLinkButton" runat="server" CssClass="btn btn-primary" PostBackUrl="~/Maestros/Clientes.aspx">Clientes</asp:LinkButton>               
                <p>
                    <asp:LinkButton ID="ProductoLinkButton" runat="server" CssClass="btn btn-primary" PostBackUrl="~/Maestros/productos.aspx">Productos</asp:LinkButton>
                </p>
                <p>
                    <asp:LinkButton ID="TipoDocumentoLinkButton" runat="server" CssClass="btn btn-primary" PostBackUrl="~/Maestros/TipoDocumento.aspx">TipoDocumento</asp:LinkButton>
                </p>
                <p>
                    <asp:LinkButton ID="TecnicoinstaladorLinkButton" runat="server" CssClass="btn btn-primary" PostBackUrl="~/Maestros/Tecnicoinstalador.aspx">Tecnico instalador</asp:LinkButton>
                </p>
                <p>
                    <asp:LinkButton ID="TipoDeServicioLinkButton" runat="server" CssClass="btn btn-primary" PostBackUrl="~/Maestros/TipoDeServicio.aspx">Tipo de servicio</asp:LinkButton>
                </p>
                <p>
                    <asp:LinkButton ID="ProveedorLinkButton" runat="server" CssClass="btn btn-primary" PostBackUrl="~/Maestros/Proveedor.aspx">Proveedor</asp:LinkButton>

                </p>
            </p>
        </div>
        <h3>Movimientos</h3>
        <div>
            <p>
                <p>
                    <asp:LinkButton ID="VentasLinkButton" runat="server" CssClass="btn btn-primary" PostBackUrl="~/Movimientos/Ventas.aspx">Ventas</asp:LinkButton>
                </p>
                <p>
                    <asp:LinkButton ID="ComprasLinkButton" runat="server" CssClass="btn btn-primary" PostBackUrl="~/Movimientos/Compras.aspx">Compras</asp:LinkButton>
                </p>

            </p>
        </div> 
           
        
    </div>  
</asp:Content>

   
