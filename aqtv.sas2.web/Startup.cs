﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(aqtv.sas2.web.Startup))]
namespace aqtv.sas2.web
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
