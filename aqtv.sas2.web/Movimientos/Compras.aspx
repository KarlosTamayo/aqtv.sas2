﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Compras.aspx.cs" Inherits="aqtv.sas2.web.WebForm6" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Compras</h1>
    <p>&nbsp;</p>
    <table style="width: 62%">
        <tr>
            <td style="width: 141px">Proveedor: * </td>
            <td>
                <asp:DropDownList ID="ProveedorDropDownList" runat="server" DataSourceID="ProvedorSqlDataSource" DataTextField="NombreComercial" DataValueField="ProveedorID" Height="27px" Width="203px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 141px">Producto: *</td>
            <td>
                <asp:DropDownList ID="ProductoDropDownList" runat="server" DataSourceID="ProductoSqlDataSource" DataTextField="Descripcion" DataValueField="ProductoID" Height="26px" Width="199px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 141px">Cantidad: *</td>
            <td>
                <asp:TextBox ID="CantidadTextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 141px">Costo: *</td>
            <td>
                <asp:TextBox ID="CostoTextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
    </table>

    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <p>
        <asp:Button ID="AgregarButton" runat="server" Text="Agregar" OnClick="AgregarButton_Click" />
        <asp:Button ID="EliminarButton" runat="server" Text="Eliminar" />
        <asp:Button ID="GrabarButton" runat="server" Text="Grabar compra" OnClick="GrabarButton_Click" />
        <br />
    </p>
    <p>
        <strong>
        <asp:Label ID="MensajeLabel" runat="server" style="font-size: x-large; color: #0000CC"></asp:Label>
        </strong>
    </p>
    <p>
        <asp:GridView ID="ComprasGridView" runat="server" AutoGenerateColumns="False" EmptyDataText="&lt;h2&gt; No se ha agregado detalle&lt;h2&gt;" Width="461px" CellPadding="4" ForeColor="#333333" GridLines="None">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:BoundField DataField="ProductoID" HeaderText="Producto ID" />
                <asp:BoundField DataField="Descripcion" HeaderText="Descripción" />
                <asp:BoundField DataField="Precio" DataFormatString="{0:C1}" HeaderText="Precio">
                <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="Cantidad" DataFormatString="{0:N1}" HeaderText="Cantidad">
                <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="Valor" DataFormatString="{0:C1}" HeaderText="Valor">
                <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>
    </p>
    <table style="width: 100%">
        <tr>
            <td style="width: 134px">Total Cantidad:</td>
            <td>
                <asp:TextBox ID="TotalCantidadTextBox" runat="server" ReadOnly="True"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 134px; height: 21px;">Total valor:</td>
            <td style="height: 21px">
                <asp:TextBox ID="TotalValorTextBox" runat="server" ReadOnly="True"></asp:TextBox>
            </td>
        </tr>
    </table>
    <p>
        <br />
        <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>
        <br />
    </p>
    <asp:SqlDataSource ID="ProvedorSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select ProveedorID, NombreComercial from Proveedor
union 
select 0,'[Seleciona Nombre Provedor]'
order by 2"></asp:SqlDataSource>
    <br />
    <asp:SqlDataSource ID="ProductoSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select ProductoID,Descripcion from Producto
union
select 0,'[Seleciona producto]'
order by 2"></asp:SqlDataSource>
    <br />
    <br />
    <br />
    <br />
    <br />

</asp:Content>
