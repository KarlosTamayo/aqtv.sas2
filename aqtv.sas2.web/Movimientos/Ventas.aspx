﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Ventas.aspx.cs" Inherits="aqtv.sas2.web.WebForm8" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Ventas</h1>
    <p>&nbsp;</p>
    <table style="width: 67%">
        <tr>
            <td style="width: 123px">Cliente: *</td>
            <td>
                <asp:DropDownList ID="ClienteDropDownList" runat="server" DataSourceID="ClienteSqlDataSource" DataTextField="Nombre" DataValueField="ClientesID" Height="27px" Width="197px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 123px">Producto: *</td>
            <td>
                <asp:DropDownList ID="ProductoDropDownList" runat="server" DataSourceID="ProductoSqlDataSource" DataTextField="Descripcion" DataValueField="ProductoID" Height="27px" Width="197px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 123px">Cantidad: *</td>
            <td>
                <asp:TextBox ID="CantidadTextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 123px">Costo: *</td>
            <td>
                <asp:TextBox ID="CostoTextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
     
    </table>
       <br />
    <p>

    <asp:Button ID="AgregarButton" runat="server" OnClick="AgregarButton_Click" Text="Agregar " />
    <asp:Button ID="EliminarButton" runat="server" Text="Eliminar" />
    <asp:Button ID="GrabarCompraButton" runat="server" Text="GrabarVenta" OnClick="GrabarCompraButton_Click" />
    </p>
        <strong>
        <asp:Label ID="MensajeLabel" runat="server" style="font-size: x-large; color: #0000CC"></asp:Label>
        </strong>
    <br />
     <br />



    <asp:GridView ID="VentaGridView" runat="server" AutoGenerateColumns="False" CellPadding="4" EmptyDataText="&lt;h2&gt;No se ha agregado detalle&lt;h2&gt;" ForeColor="#333333" GridLines="None" Width="503px">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:BoundField DataField="ProductoID" HeaderText="ProductoID" />
            <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" />
            <asp:BoundField DataField="Precio" DataFormatString="{0:N1}" HeaderText="Precio">
            <ItemStyle HorizontalAlign="Left" />
            </asp:BoundField>
            <asp:BoundField DataField="Cantidad" DataFormatString="{0:N1}" HeaderText="Cantidad">
            <ItemStyle HorizontalAlign="Left" />
            </asp:BoundField>
            <asp:BoundField DataField="Valor" DataFormatString="{0:N1}" HeaderText="Valor">
            <ItemStyle HorizontalAlign="Left" />
            </asp:BoundField>
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
    <br />
    <p>&nbsp;</p>

    <table style="width: 52%">
        <tr>
            <td class="modal-sm" style="width: 122px; height: 22px">Total Cantidad:</td>
            <td style="height: 22px">
                <asp:TextBox ID="TotalCantidadTextBox" runat="server" ReadOnly="True"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="modal-sm" style="width: 122px">Total valor:</td>
            <td>
                <asp:TextBox ID="TotalValorTextBox" runat="server" ReadOnly="True"></asp:TextBox>
            </td>
        </tr>
    </table>
    <br />
    <p>&nbsp;</p>
    <asp:SqlDataSource ID="TipoDocumentoqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select TipoDocumentoID,Descripcion from TipoDocumento
union 
select 0, '[Seleccioona tipo de documento]'
order by 2"></asp:SqlDataSource>
    <asp:SqlDataSource ID="ClienteSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select ClientesID, Nombre from Clientes
union 
select 0,'[Seleciona Nombre Cliente]'
order by 2"></asp:SqlDataSource>
    <asp:SqlDataSource ID="ProductoSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select ProductoID,Descripcion from Producto
union
select 0,'[Seleciona producto]'
order by 2"></asp:SqlDataSource>
    <br />



</asp:Content>
